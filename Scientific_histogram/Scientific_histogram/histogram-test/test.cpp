#include "pch.h"
#include <random>
#include <chrono>
#include <fstream>
#include "histogram.hpp"
#include "HistogramParser.h"
#include "PrintHistogram.h"


using namespace std;

TEST(Histogram, vector_constr)
{
	const std::vector<double> vec{1, 4, 5, 2, 3, 4, 5, 2, 1, 2, 4};
	const Histogram histogram{ vec, 5 };
}

TEST(Histogram, invalid_input_values)
{
	ASSERT_ANY_THROW(Histogram first( 2,1,3 ));
	ASSERT_ANY_THROW(Histogram second( 1,6,0 ));
}

TEST(Histogram, equal_operator)
{
	const std::vector<double> vec{ 1, 4, 5, 2, 3, 4, 5, 2, 1, 2 };
	const Histogram histogram{ vec , 6};

	const Histogram histogram1{ vec, 6 };

	EXPECT_TRUE(histogram == histogram1);
	EXPECT_TRUE(histogram == histogram);

}

TEST(Histogram, minus_operator)
{
	const std::vector<double> vec{ 1, 4, 5, 2, 3, 4, 5, 2, 1, 2 };
	const Histogram histogram{ vec , 6};

	const Histogram histogram1{ vec , 6};

	const Histogram histogram2 = histogram1 - histogram;

	for(auto it = histogram2.begin(); it != histogram2.end(); ++it)
	{
		EXPECT_EQ(*it, 0);
	}
}

TEST(Histogram, minus_operator_invalid)
{
	const std::vector<double> vec{ 1, 4, 5, 2, 3, 4, 5, 2, 1, 2 };
	const std::vector<double> vec1{ 1, 4, 5, 2, 3, 4, 6, 2, 1, 2};
	const Histogram histogram{ vec, 5 };

	const Histogram histogram1{ vec1, 6};

	EXPECT_THROW(histogram1 - histogram, invalid_argument);
}

TEST(Histogram, plus_operator)
{
	const std::vector<double> vec{ 1, 1, 2, 2, 3, 3, 4, 4, 5, 5 };
	const Histogram histogram{ vec, 5};

	const Histogram histogram1{ vec, 5};

	const Histogram histogram2 = histogram1 + histogram;

	for (auto it = (++++histogram2.begin()); it != (--histogram2.end()); ++it)
	{
		EXPECT_EQ(*it, 4);
	}
}

TEST(Histogram, plus_operator_invalid)
{
	const std::vector<double> vec{ 1, 4, 5, 2, 3, 4, 5, 2, 1, 2 };
	const std::vector<double> vec1{ 1, 4, 5, 2, 3, 4, 6, 2, 1, 2 };
	const Histogram histogram{ vec, 5 };

	const Histogram histogram1{ vec1, 6 };

	EXPECT_THROW(histogram1 + histogram, invalid_argument);
}

TEST(Histogram, empty_hist_opeartors)
{
	const Histogram histogram1{ 0,3, 3 };
	const Histogram histogram2{ 0,3, 3 };
	Histogram histogram3 = histogram1 + histogram2;
	EXPECT_EQ(histogram3.getBinCount(),3);
	for(auto it : histogram3)
	{
		EXPECT_EQ(it, 0);
	}
}

TEST(Histogram, iterators)
{
	const std::vector<double> vec{ 1, 1, 2, 2, 3, 3, 4, 4, 5, 5 };
	const Histogram histogram{ vec, 5 };

	for (auto it = (++++histogram.begin()); it != histogram.end(); ++it)
	{
		EXPECT_EQ(*it, 2);
	}

	for (int j = 2; j < histogram.getBinCount(); j++)
	{
		EXPECT_EQ(histogram[j], 2);
	}

	EXPECT_THROW(histogram[6], out_of_range);
	EXPECT_THROW(histogram[10], out_of_range);
}

TEST(Histogram, add)
{
	Histogram histogram1{ 1, 8, 9 };

	random_device rd;
	mt19937 gen(rd());

	for (int i = 0; i < 100; i++)
	{
		histogram1.add(gen() % 10);
	}

	const std::vector<double> vec1{ 1, 4, 5, 2, 3, 4, 5, -1.2, 1, 0, 20, 10.2 };

	Histogram histogram2(1, 10, 3);
	for (auto it : vec1)
	{
		histogram2.add(it);
	}

}

TEST(Histogram, create)
{
	stringstream ss("1, 2, 3, 4, 1, 2, 3, 4");
	Histogram histogram{ ParsingHistogram::parsing_histogram(ss), 4};

	for(int i =0; i < 3; i++)
	{
		EXPECT_EQ(histogram[i], 2);
	}
}

TEST(Histogram, ecreate_empty_stream)
{
	stringstream ss("");

	EXPECT_THROW(Histogram histogram( ParsingHistogram::parsing_histogram(ss), 4 ), invalid_argument);
}