#include "pch.h"
#include<iostream>
#include <string>

using namespace std;

TEST(String, copy_change)
{
	MemoryLeakDetector leakDetector;
	String str("forever");
	String str1(str);
	str.clear();

	std::cout << str << '\n';
	std::cout << str1 << '\n';

	String str2(str1);
	str2.insert(0, "inter ");

	std::cout << str1 << '\n';
	std::cout << str2 << '\n';
}

TEST(String, Constr)
{
	MemoryLeakDetector leakDetector;

	String str1;

	String str0(5, '\0');

	String st(nullptr);

	EXPECT_EQ(str0.capacity(), 5);
	EXPECT_EQ(str0.size(), 5);

	str0.insert(0, "hell");

	EXPECT_EQ(str1.size(), 0);
	EXPECT_EQ(str1.capacity(), 0);

	String str2("");

	EXPECT_EQ(str2.size(), 0);
	EXPECT_EQ(str2.capacity(), 0);

	String str3("", 4);

	EXPECT_EQ(str3.size(), 0);
	EXPECT_EQ(str3.capacity(), 0);

	String str4(0, 'c');

	EXPECT_EQ(str4.size(), 0);
	EXPECT_EQ(str4.capacity(), 0);

	String str5(1, ' ');

	EXPECT_EQ(str5.size(), 1);
	EXPECT_EQ(str5.capacity(), 1);

	String str6("fer", 5);

	EXPECT_EQ(str6.size(), 3);
	EXPECT_EQ(str6.capacity(), 3);

	String str7("fer", 2);

	EXPECT_EQ(str7.size(), 2);
	EXPECT_EQ(str7.capacity(), 2);


	String str8(str6, 1, 5);

	EXPECT_EQ(str8.size(), 2);
	EXPECT_EQ(str8.capacity(), 2);
}

TEST(String, n_constr)
{
	MemoryLeakDetector leakDetector;
	String str(5, 'a');
	cout << str << endl;
	String str1("forever", 4);
	cout << str1 << endl;
	String str2(str1, 1, 2);
	cout << str2 << endl;
}

TEST(String, constr_copy)
{
	MemoryLeakDetector leakDetector;
	String* str1 = new String("tell");
	String* str2 = new String(*str1);

	EXPECT_EQ(str2->countRef(), 2);
	EXPECT_EQ(str2->countRef(), str1->countRef());

	delete str1;

	EXPECT_EQ(str2->countRef(), 1);


	delete str2;

	String str3;
	String str4(str3);

	EXPECT_EQ(str3.size(), str4.size());
	EXPECT_EQ(str3.capacity(), str4.capacity());
}

TEST(String, reserve)
{
	MemoryLeakDetector leakDetector;
	String str("re");
	EXPECT_EQ(2, str.capacity());
	str.reserve(10);
	EXPECT_EQ(10, str.capacity());

	String str1;
	str1.reserve(10);
	EXPECT_EQ(10, str1.capacity());
	EXPECT_EQ(0, str1.size());
}

TEST(String, clear)
{
	MemoryLeakDetector leakDetector;
	String str1;
	str1.clear();
	EXPECT_EQ(str1.size(), 0);
	EXPECT_EQ(str1.capacity(), 0);

	String str2("");
	str2.clear();
	EXPECT_EQ(str2.size(), 0);
	EXPECT_EQ(str2.capacity(), 0);

	String str4(str2);
	EXPECT_EQ(str4.countRef(), 2);
	EXPECT_EQ(str2.countRef(), 2);
	str4.clear();
	EXPECT_EQ(str4.countRef(), 2);
	EXPECT_EQ(str2.countRef(), 2);
	EXPECT_EQ(str4.size(), 0);
	EXPECT_EQ(str4.capacity(), 0);

	String str3("forever");
	str3.clear();
	EXPECT_EQ(str3.size(), 0);
	EXPECT_EQ(str3.capacity(), 7);


	String str_1;
	str1.clear();
	String str_2(str_1);
	str_1.reserve(10);

	EXPECT_EQ(str_1.capacity(), 10);
	EXPECT_EQ(str_2.capacity(), 0);
}

TEST(String, new_Clear)
{
	MemoryLeakDetector leakDetector;
	String str("wwer");
	String str2(str);
	str2.clear();
	EXPECT_EQ(str2.capacity(), 0);
}

TEST(String, empty)
{
	MemoryLeakDetector leakDetector;
	String str;
	EXPECT_EQ(str.empty(), true);
	str.insert(0, "car");
	EXPECT_EQ(str.empty(), false);
	str.clear();
	EXPECT_EQ(str.empty(), true);

	String str1("");
	EXPECT_EQ(str1.empty(), true);
}

TEST(String, at)
{
	MemoryLeakDetector leakDetector;
	String str("My name is Mark");
	str.at(7) = 'e';
	EXPECT_EQ(str.at(1), 'y');
	EXPECT_EQ(str.at(4), 'a');
	EXPECT_EQ(str.at(7), 'e');
	const String str1("regeneration");
	EXPECT_EQ(str1.at(1), 'e');
	EXPECT_EQ(str1.at(12), '\0');

	String str_0;
	EXPECT_THROW(str_0.at(0), out_of_range);
}

TEST(String, massive)
{
	MemoryLeakDetector leakDetector;
	String str1("I was made for loving you");
	EXPECT_EQ(str1[25], '\0');
	EXPECT_EQ(str1[3], 'a');
	EXPECT_EQ(str1[0], 'I');
	str1[3] = 'o';
	EXPECT_EQ(str1[3], 'o');

	String str("1");
	EXPECT_EQ(str[1], '\0');
}

TEST(String, back)
{
	MemoryLeakDetector leakDetector;
	String str;
	EXPECT_THROW(str.back(), out_of_range);
	str.insert(0, "2222231411");
	EXPECT_EQ(str.back(), '1');

	String str1("forever");
	EXPECT_EQ(str1.back(), 'r');
}

TEST(String, front)
{
	MemoryLeakDetector leakDetector;
	String str(10, 'v');
	EXPECT_EQ(str.front(), 'v');
	str.clear();
	str.insert(0, "ger");
	EXPECT_EQ(str.front(), 'g');

	String str1;
	EXPECT_THROW(str1.front(), out_of_range);
}

TEST(String, concat)
{
	MemoryLeakDetector leakDetector;
	String str1("I ");
	String str2("love");

	str1 += str2;

	str1 += 'c';

	EXPECT_EQ(str1.size(), 7);
	EXPECT_EQ(str1[7], '\0');

	char a = '\0';

	str1 += a;

	cout << str1 << endl;

	EXPECT_EQ(str1.size(), 8);

	String str_1;
	String str_2;
	str_1 += str_2;
	EXPECT_EQ(str_1.empty(), true);
	EXPECT_EQ(str_1.size(), 0);
	EXPECT_EQ(str_1.capacity(), 0);

	str_2 += 'v';

	EXPECT_EQ(str_2.size(), 1);
	EXPECT_EQ(str_2.capacity(), 1);

	String str_3("forever");
	str_1 += str_3;
	EXPECT_EQ(str_1.size(), 7);
	EXPECT_EQ(str_1.capacity(), 7);
}

TEST(String, new_conc)
{
	MemoryLeakDetector leakDetector;
	String str("fort");
	str += '\0';
	EXPECT_EQ(str.size(), 5);
	EXPECT_EQ(str[4], '\0');

	String str1("goal ");
	str1 += str1;
	cout << str1 << endl;

	String str0;
	str0.reserve(10);
	str0 += "revenge";
	cout << str0 << endl;
}

TEST(String, assign)
{
	MemoryLeakDetector leakDetector;
	String str;
	String str2("forever");
	str = str2;
	EXPECT_EQ(str.size(), 7);
	EXPECT_EQ(str.capacity(), 7);
	EXPECT_EQ(str.countRef(), 2);

	String str0;
	str0 = str0;
	EXPECT_EQ(str0.countRef(), 1);
}

TEST(String, insert)
{
	MemoryLeakDetector leakDetector;

	String tmp;
	tmp.insert(0, 5, '\0');
	EXPECT_EQ(tmp.size(), 5);

	String str1("I ");
	String str2("love");

	str1.insert(2, str2);

	str1.insert(str1.size(), " you");
	str1.insert(str1.size(), 2, '!');

	std::cout << str1 << std::endl;

	String str;
	str.insert(0, "Hell");
	EXPECT_EQ(str.size(), 4);

	String str_("");
	str_.insert(0, "forever");
	EXPECT_EQ(str_.size(), 7);

	std::cout << str_ << std::endl;

	String str_1;
	String str_2;
	str_1.insert(0, str_2);
	EXPECT_EQ(str_1.size(), 0);
	str_1.insert(0, 4, 'v');
	EXPECT_EQ(str_1.size(), 4);

	std::cout << str_1 << std::endl;

	String str_3;
	String str_4("forever");
	str_4.insert(3, str_3);
	EXPECT_EQ(str_4.size(), 7);

	std::cout << str_4 << std::endl;
}

TEST(Stirng, ins_new)
{
	MemoryLeakDetector leakDetector;
	String str("123");
	str.insert(1, "456");

	str.insert(5, 6, '\0');

	EXPECT_EQ(str.size(), 12);

	cout << str << endl;

	String str0("I love");

	str0.insert(2, "don't ");
	cout << str0 << endl;

	str0.replace(2, 5, "really");

	cout << str0 << endl;

	String temp = String("heo");
	temp.insert(2, 2, 'l');
	cout << temp << endl;
}

TEST(String, erase)
{
	MemoryLeakDetector leakDetector;

	String str(10, 'a');
	str.erase(2, 5);
	EXPECT_EQ(str.size(), 5);
	str.erase();
	EXPECT_EQ(str.size(), 0);
	std::cout << str << std::endl;
	str.insert(0, "after");
	EXPECT_EQ(str.size(), 5);
	str.erase(4);
	std::cout << str << std::endl;
	EXPECT_EQ(str.size(), 4);
	str.erase(0, 10);
	EXPECT_EQ(str.size(), 0);

	String temp{ "I love" };
	temp.erase(2, 10);
	cout << temp << endl;
}

TEST(String, replace)
{
	MemoryLeakDetector leakDetector;
	String str("aaaaaaaaaaaaaaaaaaaaaa");
	str.replace(1, 3, "nnn");

	std::cout << str << std::endl;

	String str1("s");
	str1.replace(1, 4, "gert");

	std::cout << str1 << std::endl;

	String str2("e");
	str2.replace(0, 0, "ret");
	std::cout << str2 << std::endl;
	EXPECT_EQ(str2.size(), 1);

	String temp("wer");
	temp.replace(0, 10, "");
	cout << temp << endl;
}

TEST(String, new_repl)
{
	MemoryLeakDetector leakDetector;
	String str;
	str.replace(0, 5, 4, '\0');
	EXPECT_EQ(str.size(), 4);
}

TEST(String, swap)
{
	MemoryLeakDetector leakDetector;
	String str1("first");
	String str3(str1);
	String str4(str3);
	String str2("second");


	str1.swap(str2);

	EXPECT_EQ(str2.countRef(), 3);
	EXPECT_EQ(str1.countRef(), 1);

	String str;
	String str0;
	str.swap(str0);

	EXPECT_EQ(str.countRef(), 1);
	EXPECT_EQ(str0.countRef(), 1);
}

TEST(String, find)
{
	MemoryLeakDetector leakDetector;
	String str1("My new car1 worse then car2 but car3 the best");
	EXPECT_EQ(str1.find("car2"), 23);
	EXPECT_EQ(str1.find('c'), 7);
	EXPECT_EQ(str1.find('M'), 0);
	String str;
	str.find('\0');
}

TEST(String, substr)
{
	MemoryLeakDetector leakDetector;
	String str;
	String str0("terra");
	String str_1 = str0.substr(0, 0);
	cout << str_1;
	EXPECT_EQ(str.compare(str0.substr(0, 0).data()), 0);


	str.insert(0, "Hello");
	String str1 = str.substr();
	String str2 = str1.substr(2, 7);
	EXPECT_EQ(str1.compare(str), 0);
	cout << str1 << std::endl;
	cout << str2 << std::endl;

	String temp = String("I really love Varya");
	String temp1 = temp.substr(9, 20);
	cout << temp1 << endl;
}

TEST(String, size)
{
	MemoryLeakDetector leakDetector;
	String str;
	EXPECT_EQ(str.size(), 0);
	str.insert(0, "Hello");
	EXPECT_EQ(str.size(), 5);
	EXPECT_EQ(str.capacity(), 5);
}

TEST(String, init)
{
	MemoryLeakDetector leakDetector;
	String str("fer");
	String str1("der");
	str1 = str;
	EXPECT_EQ(str.compare(str1), 0);

	EXPECT_EQ(str1.countRef(), 2);
	EXPECT_EQ(str.countRef(), 2);
}

TEST(String, countref)
{
	MemoryLeakDetector leakDetector;
	String str;
	String str1(str);
	auto t = str1.countRef();
	EXPECT_TRUE(t == 2);
}

TEST(String, nulltermin)
{
	MemoryLeakDetector leakDetector;
	String str("123");
	str += '\0';
	std::cout << str << std::endl;

	String str0;
	str += '\0';
	str0 += '\0';
	EXPECT_EQ(str0.empty(), false);
}

TEST(String, assigment)
{
	MemoryLeakDetector leakDetector;
	String str;
	String str0;
	String str1("Hello");
	str = str1;
	cout << str << endl;

	str = "";
	cout << str << endl;

	str = str0;
	str = nullptr;
}

TEST(String, compare)
{
	MemoryLeakDetector leakDetector;
	String str0("guilt");
	String str1("guilty");
	String str2("guilt");

	String temp{};
	String temp1 = String(10, '\0');


	EXPECT_EQ(temp.compare(temp1), -1);
	EXPECT_EQ(str0.compare(str1), -1);
	EXPECT_EQ(str0.compare(str2), 0);
}

TEST(String, erase_new)
{
	String str = String("Hello World");
	str.erase(5);
	cout << str << endl;

	String str1 = String("I love you");
	str1.erase(2, 5);	
	cout << str1 << endl;
}