#include "prefix_tree.hpp"
#include <stack>

namespace
{
	std::unordered_map<char, size_t> convert_string(std::string input_str)
	{
		std::unordered_map<char, size_t> container;

		for (char& i : input_str)
		{
			if (container.find(i) != container.end())
			{
				container.find(i)->second++;
				continue;
			}
			container.insert(std::make_pair(i, 1));
		}

		return container;
	}
}

//static void recurs_search(std::unordered_map<char, prefix_node>& transition, std::unordered_map<char, size_t>& in_cont, std::string& temp_string, std::vector<std::string>& result_vector)
//{
//	for (auto it_one_world : transition)
//	{
//		auto iter = in_cont.find(it_one_world.first);
//
//		if (iter != in_cont.end() && iter->second > 0)
//		{
//			temp_string += it_one_world.first;
//
//			iter->second--;
//
//			if (it_one_world.second.getState() == true)
//			{
//				result_vector.push_back(temp_string);
//			}
//			recurs_search(it_one_world.second.getTransition(), in_cont, temp_string, result_vector);
//			temp_string.erase(--temp_string.end());
//		}
//
//	}

void prefix_tree::add(std::string& in_str) noexcept
{
	prefix_node* temp_node = &root;

	for(auto it : in_str)
	{
		std::unordered_map<char, prefix_node>* temp_transition = &temp_node->getTransition();

		auto is_char_it = temp_transition->find(it);

		if(is_char_it != temp_transition->end())
		{
			temp_node = &is_char_it->second;
		}
		else
		{
			temp_transition->insert(std::make_pair(it, prefix_node()));

			if (it == std::string::npos)
			{
				temp_transition->find(it)->second.setState(true);
			}

			temp_node = &temp_transition->find(it)->second;
		}
	}

	temp_node->setState(true);
}

bool prefix_tree::search_single_word(const std::string& check_string) noexcept
{
	prefix_node* temp_node = &root;

	for(auto it : check_string)
	{
		std::unordered_map<char, prefix_node>* temp = &temp_node->getTransition();

		auto num = temp->find(it);

		if (num != temp->end())
		{
			temp_node = &num->second;
		}
		else
		{
			return false;
		}
	}

	if (temp_node->getState() == true)
	{
		return true;
	}

	return false;
}

//std::vector<std::string> prefix_tree::search(const std::string& check_string) noexcept
//{
//	std::vector<std::string> result_vector;
//
//	std::string temp_string;
//
//	std::unordered_map<char, size_t> in_cont = convert_string(check_string);
//
//	for (auto& it_all_worlds : root.getTransition())
//	{
//		std::unordered_map<char, size_t> temp_cont = in_cont;
//
//		if (in_cont.find(it_all_worlds.first) != in_cont.end())
//		{
//			auto i = root.getTransition().find(it_all_worlds.first);
//			prefix_node* temp_node = &i->second;
//			temp_string += it_all_worlds.first;
//
//			recurs_search(temp_node->getTransition(), temp_cont, temp_string, result_vector);
//
//			temp_string.clear();
//		}
//	}
//
//	return result_vector;
//}

std::vector<std::string> prefix_tree::dfs_search(const std::string& check_string) noexcept
{
	std::vector<std::string> result_vector;
	std::string temp_string;
	std::unordered_map<char, size_t> conv_string = convert_string(check_string);
	std::stack<std::unordered_map<char, prefix_node>::iterator> stack;

	bool start = false;
	auto iter = root.getTransition().begin();

	for (auto& it_start : root.getTransition())
	{
		if(start != false)
		{
			++iter;
		}
		start = true;

		if(iter == root.getTransition().end())
		{
			break;
		}

		stack.push(iter);

		auto prev_it = iter->second.getTransition().begin();

		while (!stack.empty())
		{
			auto it = stack.top();

			auto word_it = conv_string.find(it->first);

			if (word_it != conv_string.end() && word_it->second > 0 && prev_it != it->second.getTransition().end())
			{
				temp_string += it->first;
				stack.push(it->second.getTransition().begin());
				word_it->second--;

				if (prev_it->second.getState() == true)
				{
					temp_string += prev_it->first;
					result_vector.push_back(temp_string);
					temp_string.erase(--temp_string.end());
				}

				prev_it = it->second.getTransition().begin()->second.getTransition().begin();
			}
			else
			{
				if (word_it != conv_string.end())
				{
					word_it->second++;
				}

				if (!temp_string.empty())
				{
					temp_string.erase(--temp_string.end());
				}
				prev_it = ++it;
				stack.pop();
			}
		}
	}

	return result_vector;
}

//void prefix_tree::init_tree(std::vector<std::string> dictionary) noexcept
//{
//	for(auto& it: dictionary)
//	{
//		add(it);
//	}
//}