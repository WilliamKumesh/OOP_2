#pragma once
#include <string>
#include <unordered_map>
#include <vector>

class prefix_node final
{
	std::unordered_map<char, prefix_node> transition;
	bool is_end_of_word = false;
public:
	prefix_node() = default;
	std::unordered_map<char, prefix_node> &getTransition() noexcept
	{
		return transition;
	}
	bool getState() noexcept
	{
		return is_end_of_word;
	}
	void setState(bool state) noexcept
	{
		is_end_of_word = state;
	}
}; 

class prefix_tree final
{
	prefix_node root;
public:
	prefix_tree() = default;
	/*void init_tree(std::vector<std::string> dictionary) noexcept;*/
	void add(std::string& in_str) noexcept;
	bool search_single_word(const std::string& check_string) noexcept;
	/*std::vector<std::string> search(const std::string& check_string) noexcept;*/
	std::vector<std::string> dfs_search(const std::string& check_string) noexcept;
};