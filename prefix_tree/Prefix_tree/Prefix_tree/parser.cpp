#include "parser.hpp"
#include <sstream>

prefix_tree create_tree(std::fstream& fin)
{
	prefix_tree prefix_tr;

	std::string temp;

	while (std::getline(fin, temp))
	{
		prefix_tr.add(temp);
	}

	return prefix_tr;
}