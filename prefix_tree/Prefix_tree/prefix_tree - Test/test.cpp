#include "pch.h"

using namespace std;

TEST(Prefix_Tree, TestName)
{
	MemoryLeakDetector leak_detector;

	prefix_node node;

	std::unordered_map<char, prefix_node> temp = node.getTransition();

	std::fstream fin;
	fin.open("dictionary.txt");

	prefix_tree tree = create_tree(fin);

	fin.close();

	std::string word;

	while (std::cin >> word)
	{
		std::vector<std::string> result = tree.dfs_search(word);

		std::cout << "Words in dictionary: " << std::endl;

		for (auto& it : result)
		{
			std::cout << it << std::endl;
		}
	}
}