#include "pch.h"

TEST(MessageEditor, create)
{
	MessageEditor editor;
}

TEST(MessageEditor, Add)
{
	MessageEditor editor;

	editor.AddNewWord("hell");

	EXPECT_EQ(editor.IncreaseWordFrequency("hell"), true);
	EXPECT_EQ(editor.IncreaseWordFrequency("holl"), false);
}