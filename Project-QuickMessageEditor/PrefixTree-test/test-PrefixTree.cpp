#include "pch.h"

TEST(PrefixTree, create)
{
	PrefixTree tree;
}

TEST(PrefixTree, add)
{
	PrefixTree tree;

	const std::vector<std::string> words_to_insert{ "hello", "hell", "interesting", "interested", "have", "handle"};

	for(const auto& it: words_to_insert)
	{
		tree.Add(it);
	}
}

TEST(PrefixTree, add_same_words)
{
	PrefixTree tree;

	tree.Add("Sky");
	tree.Add("Sky");
}

TEST(PrefixTree, empty)
{
	PrefixTree tree;

	tree.GetRoot().GetWordFrequency();

	tree.Add("");
}

TEST(PrefixTree, Delete)
{
	PrefixTree tree;

	const std::vector<std::string> words_to_insert{ "hello", "hell", "interesting", "interested", "have", "handle" };

	for (const auto& it : words_to_insert)
	{
		tree.Add(it);
	}

	EXPECT_EQ(tree.DeleteElement("hello"), true);
	EXPECT_EQ(tree.DeleteElement("hell"), true);
	EXPECT_EQ(tree.DeleteElement("interested"), true);

	EXPECT_EQ(tree.DeleteElement("helloween"), false);
}

TEST(PrefixTree, DeleteEmpty)
{
	PrefixTree tree;

	const std::vector<std::string> words_to_insert{ "hello", "hell", "interesting", "interested", "have", "handle" };

	for (const auto& it : words_to_insert)
	{
		tree.Add(it);
	}

	tree.DeleteElement("");
}

TEST(PrefixTree, Save)
{
	PrefixTree tree;

	const std::vector<std::string> words_to_insert{ "hell",  "interesting", "have", "hellsing", "horse"};

	for (const auto& it : words_to_insert)
	{
		tree.Add(it);
	}

	std::ofstream file_out("result.txt");

	tree.SaveTreeToFile(file_out);
}

TEST(PrefixTree, SaveEmpty)
{
	PrefixTree tree;

	std::ofstream file_out("result1.txt");

	tree.SaveTreeToFile(file_out);
}

TEST(PrefixTree, SaveOneLetter)
{
	PrefixTree tree;

	tree.Add("an");
	tree.Add("e");
	tree.Add("c");

	std::ofstream file_out("result2.txt");

	tree.SaveTreeToFile(file_out);
}