#include "pch.h"

using namespace std;

TEST(PrefixTreeSearcher, Search_one_word)
{
	PrefixTree trie;

	const vector<string> words_to_insert{ "have", "Hello", "has", "difference" };

	for(const auto& it: words_to_insert)
	{
		trie.Add(it);
	}


	for(const auto& it: words_to_insert)
	{
		EXPECT_EQ(PrefixTreeSearcher::SearchCertainWord(trie, it), true);
	}
}

TEST(PrefixTreeSearcher, Search_filling_word)
{
	PrefixTree trie;

	const vector<string> words_to_insert{ "have", "Hello", "horse", "difference", "dangerous"};

	for (const auto& it : words_to_insert)
	{
		trie.Add(it);
	}

	auto result = PrefixTreeSearcher::SearchFillingWords(trie, "ha");

	ASSERT_EQ(result, "ve");

	result = PrefixTreeSearcher::SearchFillingWords(trie, "ho");

	ASSERT_EQ(result, "rse");

	result = PrefixTreeSearcher::SearchFillingWords(trie, "di");

	ASSERT_EQ(result,"fference");

	result = PrefixTreeSearcher::SearchFillingWords(trie, "da");

	ASSERT_EQ(result, "ngerous");

	result = PrefixTreeSearcher::SearchFillingWords(trie, "He");

	ASSERT_EQ(result, "llo");

	result = PrefixTreeSearcher::SearchFillingWords(trie, "he");

	ASSERT_EQ(result, "");
}

TEST(PrefixTreeSearcher, Search_filling_word_same_root)
{
	PrefixTree trie;

	trie.Add("hell");
	trie.Add("hellsing");

	auto result = PrefixTreeSearcher::SearchFillingWords(trie, "he");

	ASSERT_EQ(result, "ll");

	result = PrefixTreeSearcher::SearchFillingWords(trie, "hell");

	ASSERT_EQ(result, "sing");
}