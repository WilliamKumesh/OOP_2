#pragma once
#include <string>
#include <vector>

class TextObject final
{
	std::vector<std::string> lines;
public:
	TextObject(std::istream& in);

	void deleteLines(size_t start_line_pos, size_t stop_line_pos);
	void addLines(size_t start_line_pos, const std::vector<std::string>& add_lines);

	const std::vector<std::string>& getLines() const
	{
		return lines;
	}
};