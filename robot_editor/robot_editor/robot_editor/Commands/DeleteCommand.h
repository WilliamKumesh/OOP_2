#pragma once
#include "AbstractCommand.h"

class DeleteCommand final : public AbstractCommand
{
	size_t start;
	size_t end;
	std::vector<std::string> text_for_undo;
public:
	DeleteCommand(size_t start_line, size_t end_line) : start(start_line), end(end_line)
	{}

	void perform(TextObject& text_object) override;
	void undo(TextObject& text_object) override;

	~DeleteCommand() override = default;
};