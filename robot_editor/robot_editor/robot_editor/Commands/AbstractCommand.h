#pragma once
#include "../TextObject.h"

class AbstractCommand 
{
public:
	virtual void perform(TextObject& text_object) = 0;
	virtual void undo(TextObject& text_object) = 0;

	virtual ~AbstractCommand();
};