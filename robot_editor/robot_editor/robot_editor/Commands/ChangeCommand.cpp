#include "ChangeCommand.h"
#include "iterator"

void ChangeCommand::perform(TextObject& text_object)
{
	std::copy(text_object.getLines().begin() + start, text_object.getLines().begin() + end, std::back_inserter(text_for_undo));
	text_object.deleteLines(start, end);
	text_object.addLines(start, text);
}

void ChangeCommand::undo(TextObject& text_object)
{
	text_object.deleteLines(start, start + text.size());
	text_object.addLines(start, text_for_undo);
}

