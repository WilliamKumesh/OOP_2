#include "DeleteCommand.h"
#include "iterator"

void DeleteCommand::perform(TextObject& text_object)
{
	std::copy(text_object.getLines().begin() + start, text_object.getLines().begin() + end, std::back_inserter(text_for_undo));
	text_object.deleteLines(start, end);
}

void DeleteCommand::undo(TextObject& text_object)
{
	text_object.addLines(start, text_for_undo);
}

