#include "InsertCommand.h"

void InsertCommand::perform(TextObject& text_object)
{
	text_object.addLines(start, inserted);
}

void InsertCommand::undo(TextObject& text_object)
{
	text_object.deleteLines(start, start + inserted.size());
}

