#include "ReplaceCommand.h"
#include <stdexcept>

bool ReplaceCommand::check_deleted_text(const TextObject& text_object) const
{
	size_t count_same_lines = 0;
	for (auto i = start; i < end; i++)
	{
		if (text_object.getLines()[i] == text_object.getLines()[count_same_lines])
		{
			count_same_lines++;
		}
		else
		{
			count_same_lines = 0;
		}
		if (count_same_lines == deleted.size())
		{
			return true;
		}
	}
	return false;
}

void ReplaceCommand::perform(TextObject& text_object)
{
	if(check_deleted_text(text_object) == false)
	{
		throw std::invalid_argument("No such text in file");
	}

	text_object.deleteLines(line_to_delete, line_to_delete + deleted.size());
	text_object.addLines(line_to_delete, inserted);
}

void ReplaceCommand::undo(TextObject& text_object)
{
	text_object.deleteLines(line_to_delete, line_to_delete + inserted.size());
	text_object.addLines(line_to_delete, deleted);
}
