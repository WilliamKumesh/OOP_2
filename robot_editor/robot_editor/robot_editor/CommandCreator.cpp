#include "CommandCreator.h"
#include <sstream>


std::vector<std::string> CommandCreator::TextParser(std::string& text_in)
{
	std::vector<std::string> new_text;

	const std::string tab_symbol = "\\\n";
	const std::string slash_symbol = "\\\\";
	const std::string quote_symbol = "\\\"";

	ConverterSymbol(text_in, slash_symbol, "\\");
	ConverterSymbol(text_in, quote_symbol, "\"");
	ConverterSymbol(text_in, tab_symbol, "\n");

	auto pos_start = 0;
	auto pos = text_in.find('\n');

	while(pos != std::string::npos)
	{
		std::string new_text_line = text_in.substr(pos_start, pos);
		new_text.push_back(new_text_line);
		pos_start = pos;
		pos = text_in.find('\n');
	}

	return new_text;
}

std::pair<std::string, std::string> CommandCreator::IntervalParsing(std::stringstream& s_stream, TextObject& textObj)
{
	std::string interval_first, interval_second;

	std::pair<std::string, std::string> interval;

	std::string check;
	s_stream << check;

	if (check == "from")
	{
		s_stream << interval_first;
		if (interval_first == "to")
		{
			interval_first = "0";
			std::stringstream make_second;
			make_second << textObj.getLines().size();
			std::string second = make_second.str();

			interval.first = interval_first;
			interval.second = second;

			return interval;
		}
	}
	s_stream << check;
	if (check == "to")
	{
		s_stream << interval_second;
	}

	interval.first = interval_first;
	interval.second = interval_second;

	return interval;
}

std::string CommandCreator::ParsInsertText(std::stringstream& s_stream)
{
	std::string insert_text;
	while(!s_stream.eof())
	{
		std::string word_to_insert;
		s_stream << word_to_insert;
		insert_text += word_to_insert;
		insert_text += ' ';
	}
	return insert_text;
}

CommandCreator::ParsingCommand CommandCreator::CommandParser(std::string& command_line, TextObject& textObj)
{
	ParsingCommand pars_command;

	auto pos = command_line.find('\"');
	if (pos != std::string::npos)
	{
		std::vector<size_t> positions;

		while (pos != std::string::npos)
		{
			positions.push_back(pos);
			pos = command_line.find('\"', pos);
		}

		if(!positions.empty())
		{
			
		}
	}

	std::stringstream s_stream(command_line);
	std::string command;

	s_stream << command;

	std::unordered_set<std::string> commands{ "delete", "change", "insert", "replace" };

	if(commands.find(command) != commands.end())
	{
		pars_command.setCommandName(command);
	}
	else
	{
		return  pars_command;
	}

	if(command == "insert")
	{
		std::string check;
		s_stream << check;
		if(check == "after")
		{
			s_stream << check;
			pars_command.setFirstLine(check);
		}

		pars_command.setText(ParsInsertText(s_stream));

		return  pars_command;
	}

	auto interval = IntervalParsing(s_stream, textObj);

	pars_command.setFirstLine(interval.first);
	pars_command.setSecondLine(interval.second);

	if(command == "replace")
	{
		std::string check;
		check = s_stream.str();
		auto pos = check.find('\"');
		check.erase(0, pos);
		pos = check.find('\"');

		while()
	}

	return pars_command;
}

void CommandCreator::ConverterSymbol(std::string& text, const std::string& convert_value, const std::string& new_value)
{
	auto pos = text.find(convert_value);
	while(pos != std::string::npos)
	{
		text.replace(pos, convert_value.size(), new_value);
		pos = text.find(convert_value);
	}
}

std::unique_ptr<AbstractCommand> CommandCreator::create_command(const std::string& command_line, TextObject& textObj)
{
	std::string new_com_line = command_line;

	const ParsingCommand command = CommandParser(new_com_line, textObj);

	if(command.getName() == "delete")
	{
		return std::make_unique<DeleteCommand>(std::stoi(command.getFirst()), std::stoi(command.getSecond()));
	}
}
