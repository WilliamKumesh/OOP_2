#pragma once
#include <istream>
#include <vector>
#include <stack>
#include "Commands/AbstractCommand.h"

class FileEditor final
{
	std::vector<std::string> command_lines;
	std::stack<std::unique_ptr<AbstractCommand>> commands_to_make;
	std::stack<std::unique_ptr<AbstractCommand>> command_history;
	TextObject text_object;

	void perform();
	void cancel();
public:
	FileEditor(std::istream& text_stream, std::istream& command_stream);

	std::string Start();
};