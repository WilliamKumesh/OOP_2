#include "file_editor.h"
#include <fstream>

FileEditor::FileEditor(std::istream& text_stream, std::istream& command_stream) : text_object(text_stream)
{
	std::string line;

	while(std::getline(command_stream, line))
	{
		command_lines.push_back(line);
	}
}

std::string FileEditor::Start()
{
	const std::string undo_ = "undo";
	for(const auto& str: command_lines)
	{
		if(str == undo_)
		{
			cancel();
			continue;
		}
		//std::unique_ptr<AbstractCommand> command = 
	}
	return undo_;
}

void FileEditor::perform()
{
	commands_to_make.top()->perform(text_object);
	command_history.push(commands_to_make.top());
	commands_to_make.pop();
}

void FileEditor::cancel()
{
	command_history.top()->undo(text_object);
	command_history.pop();
}
