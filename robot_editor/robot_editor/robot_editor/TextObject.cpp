#include "TextObject.h"
#include <sstream>

void TextObject::addLines(size_t start_line_pos, const std::vector<std::string>& add_lines)
{
	lines.insert(lines.begin() + start_line_pos, add_lines.begin(), add_lines.end());
}
 
void TextObject::deleteLines(size_t start_line_pos, size_t stop_line_pos)
{
	lines.erase(lines.begin() + start_line_pos, lines.begin() + stop_line_pos);
}

TextObject::TextObject(std::istream& in)
{
	std::string line;

	while(std::getline(in, line))
	{
		if(!line.empty())
		{
			lines.push_back(line);
		}
	}
}