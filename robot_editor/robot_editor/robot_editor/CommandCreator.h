#pragma once

#include <memory>
#include "unordered_set"
#include  "Commands/ChangeCommand.h"
#include "Commands/DeleteCommand.h"
#include "Commands/InsertCommand.h"
#include "Commands/ReplaceCommand.h"

class CommandCreator
{
	class ParsingCommand
	{
		std::string command_name;
		std::string first_line;
		std::string second_line;
		std::vector<std::string> text_insert;
	public:
		void setCommandName(const std::string &command_name_)
		{
			command_name = command_name_;
		}
		void setFirstLine(const std::string& first_line_)
		{
			first_line = first_line_;
		}
		void setSecondLine(const std::string& second_line_)
		{
			second_line = second_line_;
		}
		const std::string& getName() const
		{
			return command_name;
		}
		const std::string& getFirst() const
		{
			return first_line;
		}
		const std::string& getSecond() const
		{
			return second_line;
		}
		void setText((const std::string& text_)
		{
			text_insert = text_;
		}
		const std::vector<std::string>& getText() const
		{
			return text_insert;
		}
	};

	static std::pair<std::string, std::string> IntervalParsing(std::stringstream& s_stream, TextObject& textObj);
	static std::vector<std::string> TextParser(std::string& text_in);
	static  std::string ParsInsertText(std::stringstream& s_stream);
	static ParsingCommand CommandParser(std::string& command_line, TextObject& textObj);
	static void ConverterSymbol(std::string& text, const std::string& convert_value, const std::string& new_value);

public:
	CommandCreator() = delete;
	static std::unique_ptr<AbstractCommand> create_command(const std::string& command_line, TextObject& textObj);
};