#include "pch.h"

TEST(Period, constr)
{
	Date date1(5, 10, 2000);
	Date date2(1, 10, 2000);
	Period(date2, date1);
}

TEST(Period, contains)
{
	const Date date1(5, 10, 2000);
	const Date date2(1, 10, 2000);

	const Date for_check_1(2, 10, 2000);
	const Date for_check_2(3, 10, 2000);

	const Period period(date2, date1);

	EXPECT_EQ(period.CheckDateContains(for_check_1), true);
	EXPECT_EQ(period.CheckDateContains(for_check_2), true);
}

TEST(Period, invalid_contains)
{
	const Date date1(5, 10, 2000);
	const Date date2(1, 10, 2000);

	const Date for_check_1(6, 10, 2000);
	const Date for_check_2(8, 10, 2000);

	const Period period(date2, date1);

	EXPECT_EQ(period.CheckDateContains(for_check_1), false);
	EXPECT_EQ(period.CheckDateContains(for_check_2), false);
}