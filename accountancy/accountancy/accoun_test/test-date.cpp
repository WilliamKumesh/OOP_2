#include "pch.h"
#include <stdexcept>

TEST(Date, Constr)
{
	MemoryLeakDetector leakDetector;

	Date(10, 1, 2015);
	EXPECT_THROW(Date(3, 20, 2014), std::out_of_range);
	EXPECT_THROW(Date(30, 2, 2014), std::out_of_range);
	EXPECT_THROW(Date(2, 30, -1), std::out_of_range);
}

TEST(Date, leap)
{
	MemoryLeakDetector leakDetector;

	Date date1(29, 2, 2000);
	EXPECT_EQ(true, Date::IsLeapYear(2000));
}

TEST(Date, compare)
{
	MemoryLeakDetector leakDetector;

	Date date1(29, 2, 2000);
	Date date2(20, 5, 2000);
	Date date3(20, 5, 1980);
	Date date4(29, 2, 2000);
	EXPECT_EQ(date1 < date2, true);
	EXPECT_EQ(date1 > date3, true);
	EXPECT_EQ(date1 == date4, true);
}

TEST(Date, getters)
{
	Date date1(29, 2, 2000);

	EXPECT_EQ(date1.getMonth(), 2);
	EXPECT_EQ(date1.getDay(), 29);
	EXPECT_EQ(date1.getYear(), 2000);
}