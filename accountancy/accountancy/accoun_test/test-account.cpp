#include "pch.h"
#include <fstream>

using namespace std;

TEST(prser, pars)
{
	setlocale(LC_ALL, "Russian");
	setlocale(LC_NUMERIC, "C");

	MemoryLeakDetector leakDetector;

	fstream fin("test1.txt");

	vector<buy_detail> temp = pars_details(fin);

	vector<Buy>buys = create_buys(temp);

	vector<Count> counts = create_counts(buys);

	fin.close(); 

	EXPECT_EQ(counts.size(), 2);

	for(int i = 0; i < counts.size(); i++)
	{
		cout << counts[i];
	}
}

TEST(prser, pars_)
{
	setlocale(LC_ALL, "Russian");
	setlocale(LC_NUMERIC, "C");

	MemoryLeakDetector leakDetector;

	fstream fin("test.txt");

	vector<buy_detail> temp = pars_details(fin);

	vector<Buy>buys = create_buys(temp);

	vector<Count> counts = create_counts(buys);

	fin.close();

	EXPECT_EQ(counts.size(), 1);

	for (int i = 0; i < counts.size(); i++)
	{
		cout << counts[i];
	}
}

TEST(Count, func)
{
	setlocale(LC_ALL, "Russian");
	setlocale(LC_NUMERIC, "C");

	MemoryLeakDetector leakDetector;

	fstream fin("test2.txt");

	vector<buy_detail> temp = pars_details(fin);

	vector<Buy>buys = create_buys(temp);

	vector<Count> counts = create_counts(buys);

	fin.close();

	EXPECT_EQ(counts.size(), 2);

	for (int i = 0; i < counts.size(); i++)
	{
		cout << counts[i];
	}

	EXPECT_EQ(counts[0].current_sum(), 500);
	EXPECT_EQ(counts[1].current_sum(), 1000);

	Date first(3, 2, 2013);
	Date second(6, 4, 2014);
	Date third(7, 1, 2016);
	EXPECT_EQ(counts[1].addition_info(first, second), 0);
	EXPECT_EQ(counts[1].consumption_info(first, second), -4000);
	EXPECT_EQ(counts[1].consumption_info(first, third), -7000);
	EXPECT_EQ(counts[1].addition_info(second, third), 1000);
	EXPECT_EQ(counts[1].addition_info(first, third), 1000);
	EXPECT_EQ(counts[1].in_residue_info(first), 7000);
	EXPECT_EQ(counts[1].in_residue_info(second), 3000);
	EXPECT_EQ(counts[1].out_residue_info(first, second), 3000);
	EXPECT_EQ(counts[1].out_residue_info(first, third), 1000);
}