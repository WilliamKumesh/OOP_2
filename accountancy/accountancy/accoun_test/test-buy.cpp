#include "pch.h"
#include <stdexcept>

TEST(Buy, constr)
{
	setlocale(LC_ALL, "Russian");
	setlocale(LC_NUMERIC, "C");

	MemoryLeakDetector leakDetector;

	Date data(2, 2, 2000);
	CountName normal_name("Card");
	CountName empty_name("");
	EXPECT_THROW(Buy a(100,empty_name, data), std::invalid_argument);

	Buy c(1000, normal_name, data);
}

TEST(Buy, getters)
{
	setlocale(LC_ALL, "Russian");
	setlocale(LC_NUMERIC, "C");

	MemoryLeakDetector leakDetector;

	Date data(15, 5, 2005);
	CountName name{ "Card" };
	Buy buy_(1000, name, data);

	EXPECT_EQ(buy_.getSum(), 1000);
	EXPECT_EQ(buy_.getDate().getDay(), data.getDay());
	EXPECT_EQ(buy_.getDate().getMonth(), data.getMonth());
	EXPECT_EQ(buy_.getDate().getYear(), data.getYear());
	EXPECT_TRUE(buy_.getName().getName() == name.getName());
}