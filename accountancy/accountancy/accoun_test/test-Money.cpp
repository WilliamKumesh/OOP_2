#include "pch.h"
#include <stdexcept>

TEST(Money, constructor)
{
	Money money(0);
	Money money1(-2);
	Money money2(20);
}

TEST(Money, plus)
{
	const Money money(10);
	const Money money1(-20);
	EXPECT_EQ((money + money1).getSum(), -10);
}

TEST(Money, minus)
{
	const Money money(10);
	const Money money1(-20);
	EXPECT_EQ((money - money1).getSum(), 30);
}

TEST(Money, multiply)
{
	const Money money(10);
	const Money money1(-20);
	EXPECT_EQ((money * money1).getSum(), -200);
}

TEST(Money, division)
{
	Money money(0);
	Money money1(10);
	Money money2(-20);
	EXPECT_THROW(money1 / money, std::invalid_argument);
	EXPECT_EQ((money1 / money2).getSum(), -0.5);
} 