#include "pch.h"
#include <fstream>
#include <istream>

using namespace std;

TEST(CountBase, addition)
{
	MemoryLeakDetector leakDetector;

	stringstream ss("01.01.2014 Start ������ 1500     ���������� � �������� ���� 990 ���\n01.01.2014 Start �������� 7000      � �� ����� 7000"
				 "\n03.06.2014 �������� ������� 4000    �������\n06.06.2014 �������� ������� 3000    �������\n06.06.2014 ������ �������� 1000    �������");

	BankAccountSystem base = CreateBase(ss);

	Date first(2, 3, 2013);
	Date second(4, 6, 2014);
	Date third(7, 1, 2016);

	Period period1(first, second);
	Period period2(first, third);
	Period period3(second, third);

	CountName count{ "��������" };

	EXPECT_EQ(base.AdditionSum(count, period1).getSum(), 7000);
	EXPECT_EQ(base.AdditionSum(count, period3).getSum(), 1000);
	EXPECT_EQ(base.AdditionSum(count, period2).getSum(), 8000);
}

TEST(CountBase, consumption)
{
	MemoryLeakDetector leakDetector;

	stringstream ss("01.01.2014 Start ������ 1500     ���������� � �������� ���� 990 ���\n01.01.2014 Start �������� 7000      � �� ����� 7000"
		"\n03.06.2014 �������� ������� 4000    �������\n06.06.2014 �������� ������� 3000    �������\n06.06.2014 ������ �������� 1000    �������");

	BankAccountSystem base = CreateBase(ss);

	Date first(2, 3, 2013);
	Date second(4, 6, 2014);
	Date third(7, 1, 2016);

	Period period1(first, second);
	Period period2(first, third);
	Period period3(second, third);

	CountName count{ "��������" };

	EXPECT_EQ(base.ConsumptionSum(count, period1).getSum(), -4000);
	EXPECT_EQ(base.ConsumptionSum(count, period2).getSum(), -7000);
}

TEST(CountBase, in_residue)
{
	MemoryLeakDetector leakDetector;

	stringstream ss("01.01.2014 Start ������ 1500     ���������� � �������� ���� 990 ���\n01.01.2014 Start �������� 7000      � �� ����� 7000"
		"\n03.06.2014 �������� ������� 4000    �������\n06.06.2014 �������� ������� 3000    �������\n06.06.2014 ������ �������� 1000    �������");

	BankAccountSystem base = CreateBase(ss);

	Date first(2, 3, 2013);
	Date second(4, 6, 2014);
	Date third(7, 1, 2016);

	Period period2(first, third);

	CountName count{ "��������" };

	EXPECT_EQ(base.InResidueSum(count, period2).getSum(), 0);
}

TEST(CountBase, out_residue)
{
	MemoryLeakDetector leakDetector;

	stringstream ss("01.01.2014 Start ������ 1500     ���������� � �������� ���� 990 ���\n01.01.2014 Start �������� 7000      � �� ����� 7000"
		"\n03.06.2014 �������� ������� 4000    �������\n06.06.2014 �������� ������� 3000    �������\n06.06.2014 ������ �������� 1000    �������");

	BankAccountSystem base = CreateBase(ss);

	Date first(2, 3, 2013);
	Date second(4, 6, 2014);
	Date third(7, 1, 2016);

	Period period2(first, third);

	CountName count{ "��������" };

	EXPECT_EQ(base.OutResidueSum(count, period2).getSum(), 1000);
}