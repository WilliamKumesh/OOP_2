#include "pch.h"
#include <fstream>
#include "Creator.h"
#include <string>
#include <sstream>
#include "CountName.h"

class MemoryLeakDetector {
public:
    MemoryLeakDetector() {
        _CrtMemCheckpoint(&memState_);
    }

    ~MemoryLeakDetector() {
        _CrtMemState stateNow, stateDiff;
        _CrtMemCheckpoint(&stateNow);
        int diffResult = _CrtMemDifference(&stateDiff, &memState_, &stateNow);
        if (diffResult)
            reportFailure(stateDiff.lSizes[1]);
    }
private:
    void reportFailure(unsigned int unfreedBytes) {
        FAIL() << "Memory leak of " << unfreedBytes << " byte(s) detected.";
    }
    _CrtMemState memState_;
};


using namespace std;

TEST(prser, pars_invalid)
{
	setlocale(LC_ALL, "Russian");
	setlocale(LC_NUMERIC, "C");

	MemoryLeakDetector leakDetector;

	stringstream ss("01.01.2014 Start ������ 990     ���������� � �������� ���� 990 ���\n01.01.2014 Start ����� 7000      � �� ����� 7000"
				 "\n20.09.2014 ������ ������ 70     �������� �� ������\n20.09.2014 ������ ����� 500     ����� ������� �� ������"
	 "\n2014 - 09 - 21 ����� ����� 770.31    ����� ������ �� ��������\n123123123\n21.02.2014 ����� ������ 1000    ���� ������ � ���������");

	auto base = CreateBase(ss);

	CountName count1("������");

	EXPECT_EQ(base.getNumCounts(), 2);
	EXPECT_EQ(base.CurrentSum(count1).getSum(), 1420);
}

TEST(prser, pars)
{
	setlocale(LC_ALL, "Russian");
	setlocale(LC_NUMERIC, "C");

	MemoryLeakDetector leakDetector;

	stringstream ss("01.01.2014 Start ������ 990     ���������� � �������� ���� 990 ���\n20.09.2014 ������ ������ 70     �������� �� ������"
				 "\n21.09.2014 ����� ����� 770.31    ����� ������ �� ��������");

	BankAccountSystem base = CreateBase(ss);

	EXPECT_EQ(base.getNumCounts(), 1);
}

TEST(prser, pars_empty)
{
	setlocale(LC_ALL, "Russian");
	setlocale(LC_NUMERIC, "C");

	MemoryLeakDetector leakDetector;

	stringstream ss("");

	BankAccountSystem base = CreateBase(ss);

	EXPECT_EQ(base.getNumCounts(), 0);
}