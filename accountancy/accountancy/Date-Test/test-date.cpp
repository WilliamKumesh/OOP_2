#include "pch.h"
#include <stdexcept>

TEST(Date, Constr)
{
	MemoryLeakDetector leakDetector;

	Date(10, 1, 2015);
	EXPECT_THROW(Date(20, 3, 2014), std::out_of_range);
	EXPECT_THROW(Date(2, 30, 2014), std::out_of_range);
	EXPECT_THROW(Date(2, 30, -1), std::out_of_range);
}

TEST(Date, leap)
{
	MemoryLeakDetector leakDetector;

	Date date1(2, 29, 2000);
	EXPECT_EQ(true, date1.is_leap_yaer(2000));
}

TEST(Date, compare)
{
	MemoryLeakDetector leakDetector;

	Date date1(2, 29, 2000);
	Date date2(5, 20, 2000);
	Date date3(5, 20, 1980);
	Date date4(2, 29, 2000);
	EXPECT_EQ(date1.compare(date2), -1);
	EXPECT_EQ(date1.compare(date3), 1);
	EXPECT_EQ(date1.compare(date4), 0);
}

TEST(Date, getters)
{
	Date date1(2, 29, 2000);

	EXPECT_EQ(date1.getMonth(), 2);
	EXPECT_EQ(date1.getDay(), 29);
	EXPECT_EQ(date1.getYear(), 2000);
}

TEST(Date, setters)
{
	Date date;
	date.setDay(10);
	date.setMonth(3);
	date.setYear(1000);
	EXPECT_EQ(date.getDay(), 10);
	EXPECT_EQ(date.getMonth(), 3);
	EXPECT_EQ(date.getYear(), 1000);
}

TEST(Date, invalid_set)
{
	Date date;
	date.setDay(40);
	date.setMonth(13);
	date.setYear(-1);
	EXPECT_EQ(date.getDay(), 1);
	EXPECT_EQ(date.getMonth(), 1);
	EXPECT_EQ(date.getYear(), 0);
}