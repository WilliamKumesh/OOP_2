#include "pch.h"
#include <stdexcept>

TEST(Buy, constr)
{
	setlocale(LC_ALL, "Russian");
	setlocale(LC_NUMERIC, "C");

	MemoryLeakDetector leakDetector;

	Date data(2, 2, 2000);
	EXPECT_THROW(Buy a(100, "", "", data, false), std::invalid_argument);

	Buy c(1000, "���� 1", "���� 2", data, true);
}

TEST(Buy, getters)
{
	setlocale(LC_ALL, "Russian");
	setlocale(LC_NUMERIC, "C");

	MemoryLeakDetector leakDetector;

	Date data(5, 15, 2005);
	Buy buy_(1000, "���� 1", "���� 2", data, true);

	EXPECT_EQ(buy_.getSum(), 1000);
	EXPECT_EQ(buy_.getDate().getDay(), data.getDay());
	EXPECT_EQ(buy_.getDate().getMonth(), data.getMonth());
	EXPECT_EQ(buy_.getDate().getYear(), data.getYear());
	EXPECT_TRUE(buy_.getName() == "���� 1");
	EXPECT_TRUE(buy_.getTransitionName() == "���� 2");
}