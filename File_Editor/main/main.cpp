#include "FileEditor.h"
#include <sstream>

using namespace std;

int main()
{
	stringstream start("I want\nTo be\nForever\nWith you");
	FileEditor editor(start);

	///Delete

	string delete_command("delete from 1 to 3");

	auto delete_dto = ParseCommand(delete_command);

	auto creator = CommandCreator();

	editor.Perform(creator.CreateCommand(delete_dto));

	editor.TryUndo();

	///Insert

	string insert_command("insert after 1 (My name Sasha)");

	auto insert_dto = ParseCommand(insert_command);

	editor.Perform(CommandCreator::CreateCommand(insert_dto));

	editor.TryUndo();

	///Change

	string change_command("change from 1 to 20 with (Real life\\nIs coming)");

	auto change_dto = ParseCommand(change_command);

	editor.Perform(CommandCreator::CreateCommand(change_dto));

	editor.TryUndo();

	///Replace

	string replace_command("replace from 1 to 3 (To be\\nForever) with (Real life\\nIs coming)");

	auto replace_dto = ParseCommand(replace_command);

	editor.Perform(CommandCreator::CreateCommand(replace_dto));

	editor.TryUndo();

	return 0;
}