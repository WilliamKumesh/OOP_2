#include "pch.h"

using namespace std;

TEST(FileEditor, Create)
{
	stringstream stream("I want\nTo be\nMore stronger\nThan now");
	FileEditor editor(stream);
}

TEST(FileEditor, CreateEmpty)
{
	stringstream stream("");
	FileEditor editor(stream);
}

TEST(FileEditor, PerformUndo_for_Delete)
{
	stringstream stream("I want\nTo be\nMore stronger\nThan now");
	FileEditor editor(stream);

	const string delete_command("delete from 1 to 3");

	auto create_1 = CommandCreator();

	auto dto = ParseCommand(delete_command);

	editor.Perform(create_1.CreateCommand(dto));

	TextObject text = editor.GetText();

	EXPECT_EQ(text.GetLines().size(), 2);

	editor.TryUndo();

	text = editor.GetText();

	EXPECT_EQ(text.GetLines().size(), 4);

	ofstream out_file("result.txt");

	text.WriteToStream(out_file);
}

TEST(FileEditor, PerformEmpty)
{
	stringstream stream("I want\nTo be\nMore stronger\nThan now");
	FileEditor editor(stream);
}

TEST(FileEditor, Insert)
{
	stringstream stream("I want\nTo be\nMore stronger\nThan now");
	FileEditor editor(stream);

	const string insert_command("insert after 2 with (I want\\nSome more\\*)");

	auto create_1 = CommandCreator();

	auto dto = ParseCommand(insert_command);

	editor.Perform(create_1.CreateCommand(dto));

	TextObject text = editor.GetText();

	EXPECT_EQ(text.GetLines().size(), 6);

	editor.TryUndo();

	text = editor.GetText();

	EXPECT_EQ(text.GetLines().size(), 4);
}

TEST(FileEditor, Change)
{
	stringstream stream("I want\nTo be\nMore stronger\nThan now");
	FileEditor editor(stream);

	const string change_command("change from 1 to 4 with (I want\\nSome more)");

	auto create_1 = CommandCreator();

	auto dto = ParseCommand(change_command);

	editor.Perform(create_1.CreateCommand(dto));

	TextObject text = editor.GetText();

	EXPECT_EQ(text.GetLines().size(), 3);

	editor.TryUndo();

	text = editor.GetText();

	EXPECT_EQ(text.GetLines().size(), 4);
}

TEST(FileEditor, Replace)
{
	stringstream stream("I want\nTo be\nMore stronger\nThan now");
	FileEditor editor(stream);

	const string replace_command("replace from 1 to 4 (I want\\nSome more) with (To be\\nMore stronger)");

	auto create_1 = CommandCreator();

	auto dto = ParseCommand(replace_command);

	editor.Perform(create_1.CreateCommand(dto));

	TextObject text = editor.GetText();

	EXPECT_EQ(text.GetLines().size(), 4);

	editor.TryUndo();

	text = editor.GetText();

	EXPECT_EQ(text.GetLines().size(), 4);
}