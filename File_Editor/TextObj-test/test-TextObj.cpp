#include "pch.h"
#include "sstream"

using namespace std;

TEST(TextObj, Create)
{
	stringstream stream("I hate\nEverything\nAbout you");

	const TextObject text(stream);

	EXPECT_EQ(text.GetLines().size(), 3);
}

TEST(TextObj, CreateEmpty)
{
	stringstream stream("");

	const TextObject text(stream);

	EXPECT_EQ(text.GetLines().size(), 0);
}

TEST(TextObj, Delete)
{
	stringstream stream("I hate\nEverything\nAbout you");

	TextObject text(stream);

	text.TryDeleteLines(1, 2);

	EXPECT_EQ(text.GetLines().size(), 2);
}

TEST(TextObj, Insert)
{
	stringstream stream("I hate\nEverything\nAbout you");

	TextObject text(stream);

	const vector<string> text_ins{"hello", "enter"};

	text.TryAddLines(2, text_ins);

	EXPECT_EQ(text.GetLines().size(), 5);
}

TEST(TextObj, DeleteInvalid)
{
	stringstream stream("I hate\nEverything\nAbout you");

	TextObject text(stream);

	text.TryDeleteLines(5, 10);

	EXPECT_EQ(text.GetLines().size(), 3);
}

TEST(TextObj, InsertInvalid)
{
	stringstream stream("I hate\nEverything\nAbout you");

	TextObject text(stream);

	const vector<string> text_ins{ "hello", "enter" };

	text.TryAddLines(8, text_ins);

	EXPECT_EQ(text.GetLines().size(), 3);
}