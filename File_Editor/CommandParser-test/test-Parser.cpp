#include "pch.h"

using namespace std;

TEST(Parser, Create)
{
	string command_del("delete from 1 to 3");
	ParseCommand(command_del);

	string command_ins("insert after 3");
	ParseCommand(command_ins);

	string command_change("change from 1 to 3 with (send)");
	ParseCommand(command_change);

	string command_replace("replace from 1 to 3 (all) with (set)");
	ParseCommand(command_replace);
}

TEST(Parser, ParsName)
{
	string command_del("delete from 1 to 3");
	auto dto = ParseCommand(command_del);

	string command_ins("insert after 3");
	auto dto1 = ParseCommand(command_ins);

	string command_change("change from 1 to 3 with (send)");
	auto dto2 = ParseCommand(command_change);

	string command_replace("replace from 1 to 3 (all) with (set)");
	auto dto3 = ParseCommand(command_replace);

	EXPECT_EQ(dto.GetCommandName(), "delete");
	EXPECT_EQ(dto1.GetCommandName(), "insert");
	EXPECT_EQ(dto2.GetCommandName(), "change");
	EXPECT_EQ(dto3.GetCommandName(), "replace");
}

TEST(Parser, ParsInterval)
{
	string command_del("delete from 1 to 3");
	auto dto = ParseCommand(command_del);

	string command_ins("insert after 3");
	auto dto1 = ParseCommand(command_ins);

	string command_change("change to 3 with (send)");
	auto dto2 = ParseCommand(command_change);

	string command_replace("replace (all) with (set)");
	auto dto3 = ParseCommand(command_replace);

	EXPECT_EQ(dto.GetCommandStartPos(), 1);
	EXPECT_EQ(dto.GetCommandEndPos(), 3);
	EXPECT_EQ(dto1.GetCommandStartPos(), 3);
	EXPECT_EQ(dto2.GetCommandStartPos(), 0);
	EXPECT_EQ(dto2.GetCommandEndPos(), 3);
	EXPECT_EQ(dto3.GetCommandStartPos(), 0);
	EXPECT_EQ(dto3.GetCommandEndPos(), INT_MAX);
}